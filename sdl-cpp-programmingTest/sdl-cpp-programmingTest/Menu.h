#ifndef MENU_STAGE_H
#define MENU_STAGE_H

#include <cstdint>

class Menu
{
public:
	void setResolution(uint32_t width, uint32_t height) { m_width = width; m_height = height; }

	bool isVisible() const { return m_isVisible; }
	void show() { m_isVisible = true; }
	void hide() { m_isVisible = false; }

	bool startNewGame() 
	{ 
		bool startNewGame = m_newGame; 
		m_newGame = false; 
		return startNewGame; 
	}

	bool quit() const { return m_quit; }

	void update(float deltaTick);
private:
	uint32_t m_width{0};
	uint32_t m_height{0};
	bool m_isVisible{false};

	bool m_newGame{false};
	bool m_quit{false};
};

#endif // MENU_STAGE_H