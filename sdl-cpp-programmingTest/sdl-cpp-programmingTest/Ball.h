#pragma once

#include "Object.h"

class Ball : public Object 
{
public:
	virtual ~Ball() = default;

	void update(float delta) override;
	void draw(const ShapeRenderer& renderer) const override;

	float radius() const { return m_radius; }
private:
	float m_radius{8.0f};
};

