#ifndef GAME_STAGE_H
#define GAME_STAGE_H

#include "Ball.h"
#include "Block.h"
#include "ShapeRenderer.h"

#include <vector>
#include <tuple>

class Game
{
public:
	bool create(Vector2 resolution);

	void update(float deltaTick, float mouseXPos);
	void draw(const ShapeRenderer& renderer);

	bool hasFinished() const;

private:
	void addBall();
	
	struct HitTest
	{
		bool hit{false};
		Vector2 normal{};
		float distance{};
	};
	HitTest ballWallIntersect(const Ball& ball);
	HitTest ballBlockInterect(const Ball& ball, const Block& block);

private:
	Vector2 m_resolution{};

	Block m_paddle{};
	std::vector<Ball> m_balls;
	std::vector<Block> m_blocks;

	float m_ballSpeed{120.0f};
	float m_ballAccel{2.0f};
	float m_addBallTimer{0.0f};
	const float m_newBallInterval{10.0f};
};

#endif // GAME_STAGE_H