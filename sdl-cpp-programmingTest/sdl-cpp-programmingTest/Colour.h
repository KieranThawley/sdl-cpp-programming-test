#pragma once

#include <cstdint>

struct Colour
{
	uint8_t r{};
	uint8_t g{};
	uint8_t b{};
};