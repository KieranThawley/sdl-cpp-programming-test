#pragma once

#include "Vector2.h"
#include "Colour.h"

struct SDL_Renderer;

class ShapeRenderer
{
public:
	bool create(SDL_Renderer* renderer, Vector2 resolution);
	void destroy();

	void drawCircle(Vector2 center, float radius, Colour colour) const;
	void drawRect(Vector2 center, Vector2 extents, Colour colour) const;

private:
	SDL_Renderer* m_sdlRenderer{nullptr};
	Vector2 m_resolution{0.0f, 0.0f};
};

