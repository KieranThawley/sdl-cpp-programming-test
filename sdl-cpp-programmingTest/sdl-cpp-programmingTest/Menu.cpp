#include "Menu.h"

#include <imgui.h>

void Menu::update(float deltaTick) 
{
	if(!m_isVisible || m_width == 0 || m_height == 0) {
		return;
	}

	int menuWidth = m_width / 4;
	int menuHeight = m_width / 8;
	ImGui::SetNextWindowSize({ float(menuWidth), float(menuHeight) });
	ImGui::SetNextWindowPos({ float(m_width / 2 - menuWidth / 2), float(m_height / 2 - menuHeight / 2) });
	ImGui::Begin("Break Out", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse);
	if(ImGui::Button("New Game", { ImGui::GetWindowContentRegionWidth(), 0 }))
	{
		m_newGame = true;
	}
	ImGui::Spacing();
	if (ImGui::Button("Quit", { ImGui::GetWindowContentRegionWidth(), 0 }))
	{
		m_quit = true;
	}
	ImGui::End();
}