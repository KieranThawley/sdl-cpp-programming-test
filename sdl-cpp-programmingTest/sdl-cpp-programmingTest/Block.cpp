#include "Block.h"

void Block::update(float deltaTick)
{
}

void Block::draw(const ShapeRenderer& renderer) const
{
	renderer.drawRect(position(), m_size, m_colour);
}