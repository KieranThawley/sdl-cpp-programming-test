#pragma once

#include <cstdint>
#include <cmath>

struct Vector2
{
	static float dot(const Vector2& v1, const Vector2& v2)
	{
		return v1.x * v2.x + v1.y * v2.y;
	}

	static Vector2 normalise(const Vector2& vec)
	{
		auto mag = sqrtf(dot(vec, vec));
		return Vector2{vec.x / mag, vec.y / mag};
	}

	static Vector2 reflect(Vector2 vec, Vector2 normal)
	{
		auto n = normalise(normal);
		auto mag = 2.0f * dot(vec, normal);
		return Vector2{vec.x - normal.x * mag, vec.y - normal.y * mag};
	}

	float x;
	float y; 
};
