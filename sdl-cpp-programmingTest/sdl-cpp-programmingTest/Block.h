#pragma once

#include "Object.h"
#include "Colour.h"

class Block : public Object
{
public:
	void setSize(const Vector2& size) { m_size = size; }
	void setColour(const Colour& colour) { m_colour = colour; }

	void update(float delta) override;
	void draw(const ShapeRenderer& renderer) const override;

	const auto& size() const { return m_size; }
	const auto& colour() const { return m_colour; }
private:
	Vector2 m_size{ 80.0f, 15.0f };
	Colour m_colour{};
};

