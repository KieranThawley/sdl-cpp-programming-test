#include "ShapeRenderer.h"

#include <SDL.h>

#include <cmath>

bool ShapeRenderer::create(SDL_Renderer* renderer, Vector2 resolution)
{
	m_sdlRenderer = renderer;
	if(!m_sdlRenderer)
		return false;

	m_resolution = resolution;
	if(m_resolution.x <= 0.0f || m_resolution.y <= 0.0f)
		return false;

	return true;
}

void ShapeRenderer::destroy()
{
}

void ShapeRenderer::drawCircle(Vector2 center, float radius, Colour colour) const
{
	const uint32_t divisions = 32;
	const float two_pi = float(M_PI) * 2.0f;
	const float division_radians = two_pi / divisions;
	for(uint32_t i = 0; i < divisions; ++i)
	{
		float angle1 = division_radians * i;
		float angle2 = division_radians * (i + 1);
		float x1 = center.x + sinf(angle1) * radius;
		float y1 = center.y + cosf(angle1) * radius;
		float x2 = center.x + sinf(angle2) * radius;
		float y2 = center.y + cosf(angle2) * radius;

		SDL_SetRenderDrawColor(m_sdlRenderer, colour.r, colour.g, colour.b, SDL_ALPHA_OPAQUE);
		SDL_RenderDrawLine(m_sdlRenderer, 
			int32_t(x1), int32_t(y1),
			int32_t(x2), int32_t(y2)
		);
	}
}

void ShapeRenderer::drawRect(Vector2 center, Vector2 extents, Colour colour) const
{
	SDL_SetRenderDrawColor(m_sdlRenderer, colour.r, colour.g, colour.b, SDL_ALPHA_OPAQUE);
	auto rect = SDL_FRect{
		center.x - extents.x / 2,
		center.y - extents.y / 2,
		extents.x,
		extents.y
	};
	SDL_RenderFillRectF(m_sdlRenderer, &rect);

}