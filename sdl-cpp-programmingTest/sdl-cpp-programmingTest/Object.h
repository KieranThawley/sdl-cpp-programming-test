#pragma once

#include "Vector2.h"
#include "ShapeRenderer.h"

class Object
{
public:
	virtual ~Object() = default;

	void setPosition(Vector2 pos) { m_position = pos; }
	const Vector2& position() const { return m_position; }

	void setVelocity(Vector2 vel) { m_velocity = vel; }
	const Vector2& velocity() const { return m_velocity; }

	void applyVelocity(float deltaTick) 
	{
		m_position.x += m_velocity.x * deltaTick;
		m_position.y += m_velocity.y * deltaTick;
	}

	virtual void update(float delta) = 0;
	virtual void draw(const ShapeRenderer& renderer) const = 0;
private:
	Vector2 m_position;
	Vector2 m_velocity;

};