#include <SDL.h>
#include <stdio.h>
#include <memory>

#include <imgui.h>
#include <imgui_sdl.h>

#include "Menu.h"
#include "Game.h"
#include "ShapeRenderer.h"

const int SCREEN_HEIGHT = 500;
const int SCREEN_WIDTH = 750;

int main( int argc, char* args[] )
{
	SDL_Window* gameWindow = NULL;
	
	SDL_Surface* screenSurface = NULL;


	// Error check that SDL is initialised correctly
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialise! SDL_Error: %s\n", SDL_GetError() );
	}
	else
	{
		// Create the game window
		gameWindow = SDL_CreateWindow( "sdl c++ programming test", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if(gameWindow == NULL )
		{
			printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
		}
		else
		{
			SDL_Renderer* renderer = SDL_CreateRenderer(gameWindow, -1, SDL_RENDERER_ACCELERATED);

			ImGui::CreateContext();
			ImGuiSDL::Initialize(renderer, SCREEN_WIDTH, SCREEN_HEIGHT);

			const auto resolution = Vector2{ float(SCREEN_WIDTH), float(SCREEN_HEIGHT) };

			auto menu = std::make_unique<Menu>();
			menu->setResolution(SCREEN_WIDTH, SCREEN_HEIGHT);
			menu->show();

			auto game = std::make_unique<Game>();
			if (!game->create(resolution))
			{
				printf("Failed to create initial game object\n");
				return 1;
			}


			auto shapeRenderer = std::make_unique<ShapeRenderer>();
			if(!shapeRenderer->create(renderer, resolution))
			{
				printf("Failed to create shape renderer\n");
				return 1;
			}

			bool running = true;

			auto lastTick = SDL_GetTicks();
			
			SDL_Event sdlEvent;
			while(running) 
			{
				auto& io = ImGui::GetIO();

				while(SDL_PollEvent(&sdlEvent))
				{
					if(sdlEvent.type == SDL_QUIT)
					{
						running = false;
					}
					if(sdlEvent.type == SDL_WINDOWEVENT)
					{
						if (sdlEvent.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
						{
							io.DisplaySize.x = static_cast<float>(sdlEvent.window.data1);
							io.DisplaySize.y = static_cast<float>(sdlEvent.window.data2);
						}
					}
				}

				const auto now = SDL_GetTicks();
				const auto deltaTime = float(now - lastTick) / 1000.0f;
				lastTick = now;


				int mouseX, mouseY;
				const int buttons = SDL_GetMouseState(&mouseX, &mouseY);

				if(deltaTime > 0.0f)
					io.DeltaTime = deltaTime;
				io.MousePos = ImVec2(static_cast<float>(mouseX), static_cast<float>(mouseY));
				io.MouseDown[0] = buttons & SDL_BUTTON(SDL_BUTTON_LEFT);
				io.MouseDown[1] = buttons & SDL_BUTTON(SDL_BUTTON_RIGHT);

				ImGui::NewFrame();

				menu->update(deltaTime);

				if(menu->quit()) 
				{
					running = false;
				}

				if(menu->startNewGame())
				{
					menu->hide();
					// start a new game
					game = std::make_unique<Game>();
					if(!game->create(resolution))
					{
						printf("Failed to create new game object");
						running = false;
						continue;
					}
				}

				if(!menu->isVisible() && game) 
				{
					game->update(deltaTime, float(mouseX));
					if(game->hasFinished()) 
					{
						menu->show();
					}
				}

				SDL_SetRenderDrawColor(renderer, 114, 144, 154, SDL_ALPHA_OPAQUE);
				SDL_RenderClear(renderer);

				if(game)
				{
					game->draw(*shapeRenderer);
				}

				ImGui::Render();
				ImGuiSDL::Render(ImGui::GetDrawData());

				SDL_RenderPresent(renderer);
			}

			ImGuiSDL::Deinitialize();
			ImGui::DestroyContext();

			SDL_DestroyRenderer(renderer);
			SDL_DestroyWindow(gameWindow);
		}
	}

	return 0;
}