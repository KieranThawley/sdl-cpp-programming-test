#include "Game.h"

#include <cmath>
#include <cstdlib>
#include <time.h>

bool Game::create(Vector2 resolution)
{
	srand(uint32_t(time(nullptr)));

	m_resolution = resolution;

	m_paddle.setPosition(Vector2{resolution.x / 2.0f, resolution.y - resolution.y / 6.0f});
	m_paddle.setSize(Vector2{ 80.0f, 15.0f });
	m_paddle.setColour(Colour{ 150, 200, 255 });

	addBall();

	uint32_t blockWCount = 16;
	uint32_t blockHCount = 8;
	float blockWidth = m_resolution.x / float(blockWCount);
	float blockHeight = 15.0f;
	float heightOffset = 50.0f;
	const Colour colours[] = {
		Colour{255, 0, 0},
		Colour{255, 130, 0},
		Colour{255, 255, 0},
		Colour{0, 255, 0},
		Colour{0, 255, 255},
		Colour{0, 0, 255},
		Colour{130, 0, 255},
		Colour{255, 0, 255},
	};
	const auto colourCount = sizeof(colours) / sizeof(Colour);
	for (uint32_t x = 0; x < blockWCount; ++x)
	{
		float xPos = x * blockWidth + blockWidth / 2;
		for (uint32_t y = 0; y < blockHCount; ++y)
		{
			float yPos = y * blockHeight + blockHeight / 2 + heightOffset;
			Block block{};
			block.setPosition(Vector2{ xPos, yPos });
			block.setSize(Vector2{ blockWidth - 2, blockHeight - 2 });
			block.setColour(colours[y % colourCount]);
			m_blocks.emplace_back(std::move(block));
		}
	}

	return true;
}

void Game::update(float deltaTick, float mouseXPos)
{
	// Update paddle
	auto paddleX = mouseXPos;
	const auto halfPaddleWidth = m_paddle.size().x / 2.0f;
	if(paddleX - halfPaddleWidth < 0.0f)
	{
		paddleX = halfPaddleWidth;
	}
	else if(paddleX + halfPaddleWidth > m_resolution.x) {
		paddleX = m_resolution.x - halfPaddleWidth;
	}

	m_paddle.setPosition(Vector2{ paddleX, m_paddle.position().y});
	m_paddle.update(deltaTick);

	// update balls
	m_ballSpeed += m_ballAccel * deltaTick;
	
	auto ballIt = m_balls.begin();
	while(ballIt != m_balls.end()) 
	{
		auto& ball = *ballIt;
		ball.update(deltaTick);
		if(ball.position().y + ball.radius() > m_resolution.y)
		{
			ballIt = m_balls.erase(ballIt);
			continue;
		}

		auto result = ballWallIntersect(ball);

		const auto paddleHitResult = ballBlockInterect(ball, m_paddle);
		if (paddleHitResult.hit) {
			result.hit = true;
			result.normal.x += paddleHitResult.normal.x;
			result.normal.y += paddleHitResult.normal.y;
			if(paddleHitResult.distance > result.distance) 
			{
				result.distance = paddleHitResult.distance;
			}
		}

		auto blockIt = m_blocks.begin();
		while(blockIt != m_blocks.end())
		{
			const auto blockHitResult = ballBlockInterect(ball, *blockIt);
			if (blockHitResult.hit) {
				result.hit = true;
				result.normal.x += blockHitResult.normal.x;
				result.normal.y += blockHitResult.normal.y;
				if (blockHitResult.distance > result.distance)
				{
					result.distance = blockHitResult.distance;
				}
				blockIt = m_blocks.erase(blockIt);
			}
			else {
				++blockIt;
			}
		}

		if (result.hit) {
			const auto newNormal = Vector2::normalise(Vector2::reflect(ball.velocity(), result.normal));
			auto newVelocity = newNormal;
			newVelocity.x *= m_ballSpeed;
			newVelocity.y *= m_ballSpeed;
			ball.setVelocity(newVelocity);
			auto position = ball.position();
			position.x += newNormal.x * result.distance;
			position.y += newNormal.y * result.distance;
			ball.setPosition(position);

		}

		++ballIt;
	}

	// add new balls
	m_addBallTimer += deltaTick;
	if(m_addBallTimer > m_newBallInterval) {
		m_addBallTimer = 0.0f;
		addBall();
	}
}

void Game::draw(const ShapeRenderer& renderer)
{
	for(const auto& ball : m_balls)
	{
		ball.draw(renderer);
	}
	for (const auto& block : m_blocks)
	{
		block.draw(renderer);
	}

	m_paddle.draw(renderer);
}

bool Game::hasFinished() const {
	return m_balls.empty() || m_blocks.empty();
}

void Game::addBall()
{
	Ball ball;
	// make sure ball starts off going up
	auto startVelocity = Vector2::normalise(Vector2{ float(rand() % 50) - 25.0f, float(rand() % 10) + 15.0f });
	startVelocity.x *= m_ballSpeed;
	startVelocity.y *= -m_ballSpeed;
	ball.setPosition(Vector2{ m_resolution.x / 2.0f, m_resolution.y - m_resolution.y / 5.0f });
	ball.setVelocity(startVelocity);
	m_balls.emplace_back(std::move(ball));
}

Game::HitTest Game::ballWallIntersect(const Ball& ball)
{
	bool hit{false};
	Vector2 normal{};
	float distance{};
	if(ball.position().x - ball.radius() < 0.0f)
	{
		normal.x += 1.0f;
		hit = true;
		distance = ball.position().x - ball.radius();
	}
	else if(ball.position().x + ball.radius() > m_resolution.x)
	{
		normal.x -= 1.0f;
		hit = true;
		distance = m_resolution.x - ball.position().x + ball.radius();
	}
	if(ball.position().y - ball.radius() < 0.0f)
	{
		normal.y = 1.0f;
		hit = true;
		distance = ball.position().y - ball.radius();
	}

	return HitTest{hit, normal};

}

Game::HitTest Game::ballBlockInterect(const Ball& ball, const Block& block)
{
	bool hit{ false };
	Vector2 normal{ 0.0f, 0.0f };

	const auto cx = ball.position().x;
	const auto cy = ball.position().y;

	const auto x1 = block.position().x - block.size().x / 2;
	const auto x2 = block.position().x + block.size().x / 2;
	const auto y1 = block.position().y - block.size().y / 2;
	const auto y2 = block.position().y + block.size().y / 2;

	// temporary variables to set edges for testing
	float testX = cx;
	float testY = cy;

	// which edge is closest?
	if (cx < x1)      { testX = x1; normal.x = 1.0f; }  // test left edge
	else if (cx > x2) { testX = x2; normal.x = -1.0f; } // right edge
	if (cy < y1)      { testY = y1; normal.y = -1.0f; } // top edge
	else if (cy > y1) { testY = y2; normal.y = 1.0f; }  // bottom edge

	// get distance from closest edges
	float distX = cx - testX;
	float distY = cy - testY;
	float distance = sqrt((distX * distX) + (distY * distY));

	// if the distance is less than the radius, collision!
	if (distance <= ball.radius()) {
		hit = true;
	}
	return HitTest{hit, normal, distance};
}