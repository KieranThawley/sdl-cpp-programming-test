#include "Ball.h"

void Ball::update(float deltaTick)
{
	applyVelocity(deltaTick);
}

void Ball::draw(const ShapeRenderer& renderer) const
{
	renderer.drawCircle(position(), m_radius, Colour{255, 255, 255});
}